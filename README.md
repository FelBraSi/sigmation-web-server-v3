Protocolo MQTT sigmation 3.0
GENERAL

/sigmation/__<appname>/____<resource>/
NOTA: el uso de guiones bajos es reservado, no deben usarse dentro de los parametros en ninguna parte del sistema.

__<appname>
Nombre del APP a usar para el topic, el cual diferencia variables de distintos sistemas. Se forma por dos guiones bajos y el nombre del recurso.
Nombres a usar (siempre en mayusculas):

-DRS	: sistema drs
-TNT	: sistema tag
-VEN	: sistema ventilacion
-ARD	: diagnostico remoto amplificadores
-DOLF	: data over leaky feeder

__<resource>
Nombre del recurso del sistema en cuestion, Se forma por cuatro guiones bajos y el nombre del recurso. pueden ser:

-sys	: parametros del sistema
-dev	: dispositivos
-mod	: modulos

PARAMETROS
Variables
Topic: .../____<resource>/...

Se usan despues de resource, y contienen los valores, nombres, y otros datos de cada parametro, y se ordenan de la siguiente forma:

____<resource>/<resourcename>/__<var>/<varname>/ <par>       msg: value

<resourcename>	: Nombre del recurso (ej: numero serial de un equipo)
<varname>		: Nombre de la variable [ej: alarm]
__<var>		: Separador para variables (var,group,resourcegroup)
__meta		: metadata del equipo, tipo OBJ (ej: {var1: val1, var2:val2 ...})
<par>			: parametro de la variable




SubVariables
Topic: .../__var/<varname>/ __subvalue/__var/<varname>/ <par>   msg: value

Encadenamiento de subvalores dentro de un valor
Sigue la misma estructura de un valor (current/set/value)
Se puede continuar encadenando variables.

<varname>	: Nombre de la variable [ej: alarm]
__var		: Separador para variables 
<par>		: parametro de la variable

Tipos de Parametros de Variables <par>
Se encuentran luego del separador /__var/<varname>/, las siguientes son reservadas para cada variable:

__name	:  nombre amigable de la variable (ej: “Alarma de Consumo”)
__class	:  tipo de variable (Number, Text, Boolean, Alarm, group,resourcegroup)
__limited	:  valores que puede tomar la variable son limitados a ciertos valores
__options	:  listado de valores que puede tomar la variable/parametro (si es tipo limited)
__type		:  clase de variable (ver descripcion Variables)
			__type/current: Valor seteado en recurso <-Obligatorio si no es subval
			__type/set: valor a setear en recurso  <- Si se puede setear
			__type/value: valor del recurso <- Si ademas adquiere un valor
			__type/group: encadena otras subvariables (tipo:group)
			__type/resourcegroup: recurso encadenado, repite variables
__unit		: Si es class Number, define la unidad de medida (ej: [m], [dBm], [V])
__active	: boolean, indica si variable esta activa o si se debe ignorar


DRS

Topic: /sigmation/__drs/__<resource>/…


Resources:

Topic: /sigmation/__drs/__dev/<serial>/__var/

Dispositivos DRS, identificado por numero serial.
Sus parametros se definen en los siguientes grupos:

deviceinfo/
__name	:  Info. de dispositivo
__class	:  group
__limited	:  false
__options	:  
__type		:  group

networkpar/
__name	:  Parametros de red
__class	:  group
__limited	:  false
__options	:  
__type		:  group

workingpar/
__name	:  Parametros de Trabajo
__class	:  group
__limited	:  false
__options	:  
__type		:  group

alarmpar/
__name	:  Parametros de Alarma
__class	:  group
__limited	:  false
__options	:  
__type		:  group
 limitpar/
__name	:  Limtes para Alarmas
__class	:  group
__limited	:  false
__options	:  
__type		:  group

settingpar/
__name	:  Parametros de dispositivo
__class	:  group
__limited	:  false
__options	:  
__type		:  group

opticport/
__name	:  Parametros de dispositivo
__class	:  resourcegroup
__limited	:  false
__options	:  
__type		:  resourcegroup


status/  (online y otros)
__name	:  Status
__class	:  group
__limited	:  false
__options	:  
__type		:  group


Parametros por Grupo

deviceinfo/__var/

devicetype/

__name	:  Tipo de dispositivo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current

devicemodel/

__name	:  Modelo de dispositivo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current

deviceserial/

__name	:  Serial de dispositivo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current

channelnum/

__name	:  Numero de Canal
__class	:  Text
__limited	:  false
__options	:  
__type		:  current
swversion/

__name	:  Version de Software
__class	:  Text
__limited	:  false
__options	:  
__type		:  current

slavenum/

__name	:  Numero de Esclavo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current


networkpar/__var/

sitenumber/

__name	:  Numero de Sitio
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set ( __type	/current : msg   y   __type/set  : msg)

subdevicenum/

__name	:  Numero Subdispositivo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


mcipaddress/

__name	:  IP Centro Monitoreo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


mcipport/

__name	:  Puerto Centro Monitoreo
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


psprotocol/

__name	:  Protocolo PS
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


repeaterip/

__name	:  IP Repetidor
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


repeateripport/

__name	:  Puerto Repetidor
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


repeatermask/

__name	:  Mascara Red Repetidor
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


repeatergw/

__name	:  GateWay Repetidor
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


macaddress/

__name	:  MAC Repetidor
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


comstyle/

__name	:  Estilo Comm.
__class	:  Text
__limited	:  false
__options	:  
__type		:  current, set


workingpar/__var/

downlinkpow/

__name	:  Potencia Entrada DownLink
__class	:  Number
__limited	:  false
__options	:  
__unit		:  [dBm]
__type		:  current, set 

uplinkpow/

__name	:  Potencia Salida UpLink
__class	:  Number
__limited	:  false
__options	:  
__unit		:  [dBm]
__type		:  current, set 
 
alarmpar/__var/

supplypower/

__name	:  Potencia de Entrada
__class	:  Alarm
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set,value
 
masterslavelink/

__name	:  Link Maestro-Esclavo
__class	:  Alarm
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set,value
 
downlinkover/

__name	:  Potencia Entrada DownLink Sobre Limite
__class	:  Alarm
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set,value
 
downlinklow/

__name	:  Potencia Entrada DownLink Bajo Limite
__class	:  Alarm
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set,value
 

limitpar/__var/

dlinputlmin/

__name	:  Poder Entrada DownLink Mínimo
__class	:  Number
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set
 
dlinputlmax/

__name	:  Poder Entrada DownLink Máximo
__class	:  Number
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set
  
alarmdelay/

__name	:  Retardo de Alarma
__class	:  Number
__limited	:  true
__options	:  0-20
__unit		:  [s]
__type		:  current, set
 

settingpar/__var/

rfpowerswitch/

__name	:  Switch de Poder RF
__class	:  Text
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set
 
chfreq/

__name	:  Frecuencias por Canal
__class	:  group
__limited	:  false
__options	:  
__unit		:  
__type		:  group
 	
	chfreq/__var/<#>  (#: numero de canal, 1-16)
	__name	:  Canal  # 
__class	:  Number
__limited	:  false
__options	:  
__unit		:  [Mhz]
__type		:  current, set
 
uplinkatt/

__name	:  UpLink ATT
__class	:  Number
__limited	:  false
__options	:  
__unit		:  [dB]
__type		:  current, set
 
downlinkatt/

__name	:  DownLink ATT
__class	:  Number
__limited	:  false
__options	:  
__unit		:  [dB]
__type		:  current, set
 
channelswitch/

__name	:  Channel Switch
__class	:  Text
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set
 
choicemode/

__name	:  Seleccion de Modo de Trabajo
__class	:  Text
__limited	:  false
__options	:  
__unit		:  
__type		:  current, set


status/__var/

online/

__name	:  En Linea
__class	:  Alarm
__limited	:  false
__options	:  
__unit		:  
__type		:  current,value (seteado por defecto por sistema, no se puede apagar)
 
opticport/__dev/

Ver Parametros por Grupo
Parametros se repiten
 
