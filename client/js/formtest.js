import {Team} from '../../lib/collections/team.js';
import { Images } from '../../lib/collections/images.js';
import { Repetidores } from '../../lib/collections/repetidores.js';
import { OpticPortDevices } from '../../lib/collections/repetidores.js';

Template.displayElement.onCreated(function () {
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.displayElementNE.onCreated(function () {
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.elementForm.onCreated(function () {
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.formelement.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.repeaterstatics.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.repeaterconfigs.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.subelement.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.opticportform.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.opticportbox.onCreated(function(){
    Meteor.subscribe('repetidores');
    Meteor.subscribe('opticportdevices');
});

Template.repeaterstatics.helpers({
    formCollection() {
        return Repetidores;
    },
});

Template.opticportform.helpers({
    formCollection() {
        return OpticPortDevices;
    },
});

Template.opticportbox.helpers({
    formCollection(portnum) {
        return OpticPortDevices.find({idEquipo:this.current_id},{opticPort:portnum});;
    },
});

Template.repeaterconfigs.helpers({
    formCollection() {
        return Repetidores;
    },
    inprocess(thisvalue) {
        if(thisvalue>0) return true;
        else return false;
    },
});

Template.confsubmitbutton.helpers({
    inprocess(thisvalue) {
        if(thisvalue>0) return true;
        else return false;
    },
});


Template.subelement.helpers({
  displayelement(){
      var myelement = Repetidores.findOne({_id:this.current_id});
      return myelement;
  },
  formCollection() {
      return Repetidores;
  },
});

Template.formelement.helpers({
  displayelement(){
      var myelement = Repetidores.findOne({_id:this.current_id});
      return myelement;
  },
  formCollection(portnum) {
      return OpticPortDevices.find({idEquipo:this.current_id},{opticPort:portnum});;
  },
  editenable(){
      try {
        var thisedit = this.edit;
        return myelement;
      } catch (e) {
        return false;
      }
  },
  infodisp(){
      const counting = Repetidores.findOne({_id:this.current_id});
      const counter = counting.Settings.deviceinfo;
      return counter;
  },
  opticport1q(){
      try {
        const counting = Repetidores.findOne({_id:this.current_id});
        const counter = counting.opticport1.opticportdevice.length;
        return counter;
      }
      catch(err) {
        return 0;
      }

  },
  opticport2q(){
    try {
      const counting = Repetidores.findOne({_id:this.current_id});
      const counter = counting.opticport2.opticportdevice.length;
      return counter;
    }
    catch(err) {
      return 0;
    }

  },
  opticport3q(){
    try {
      const counting = Repetidores.findOne({_id:this.current_id});
      const counter = counting.opticport3.opticportdevice.length;
      return counter;
    }
    catch(err) {
      return 0;
    }

  },
  opticport4q(){
    try {
      const counting = Repetidores.findOne({_id:this.current_id});
      const counter = counting.opticport4.opticportdevice.length;
      return counter;
    }
    catch(err) {
      return 0;
    }

  },
  opticportsq(){
      var counting;
      var counter1 = 0;
      var counter2 = 0;
      var counter3 = 0;
      var counter4 = 0;

      try {
        counting =  Repetidores.findOne({_id:this.current_id});
        try {
          counter1 = counting.opticport1.opticportdevice.length;
        }
        catch(err) {
          //do nothing
        }
        try {
          counter2 = counting.opticport2.opticportdevice.length;
        }
        catch(err) {
          //do nothing
        }
        try {
          counter3 = counting.opticport3.opticportdevice.length;
        }
        catch(err) {
          //do nothing
        }
        try {
          counter4 = counting.opticport4.opticportdevice.length;
        }
        catch(err) {
          //do nothing
        }
      } catch(err) {
        //do nothing
      }


      const counter = counter1 + counter2  + counter3  + counter4 ;
      return counter;
  },
  imageFile(pictureId){
      //const fileRef = Images.collection.findOne(pictureId);
      //return Images.link(fileRef, 'thumbnail','/');
      return null;
  },
  fullImageFile(pictureId){
      //var fileRef = Images.collection.findOne(pictureId);
      //return Images.link(fileRef, 'original','/');
      return null;
  },

});

Template.displayElement.helpers({
    formCollection() {
        return Repetidores;
    },
    team(){
        var localcoll = Repetidores.find({});
        //localcoll= {uno:{_id:1},dos:{_id:2},tres:{_id:3}};
        console.log("localcoll:");
        console.log(localcoll);
        return localcoll;
    },
    alerts(thisId){
        var thisRepeater = Repetidores.findOne({_id:thisId});
        //console.log(thisRepeater);
    },
    imageFile(pictureId){
        const fileRef = Images.collection.findOne(pictureId);
        return Images.link(fileRef, 'thumbnail','/');
    },
    fullImageFile(pictureId){
        var fileRef = Images.collection.findOne(pictureId);
        return Images.link(fileRef, 'original','/');
    },
});

Template.opticportbox.helpers({
    displaythis(opticportbox){
        console.log(opticportbox);
    },
});



Template.displayElement.events({
    'click #deleteTeamMember'(){
      var r = confirm("Borrar?");
      if (r == true) {
        //Images.remove(this.picture);
        //Team.remove(this._id);
        Team.update({_id:this._id},{$set:{dontShowThis:true}});
        alert("borrado");
      }
    },
    'click #editTeamMember'(){
        console.log("insert edit action here, programmer!");
    }
});

Template.displayElementNE.helpers({
    formCollection() {
        return Repetidores;
    },
    team(){
        return Repetidores.find({});
    },
    imageFile(pictureId){
        const fileRef = Images.collection.findOne(pictureId);
        return Images.link(fileRef, 'thumbnail','/');
    },
    fullImageFile(pictureId){
        var fileRef = Images.collection.findOne(pictureId);
        return Images.link(fileRef, 'original','/');
    },
});

Template.displayElementNE.events({
    'click #deleteTeamMember'(){
      var r = confirm("Borrar?");
      if (r == true) {
        //Images.remove(this.picture);
        //Team.remove(this._id);
        Team.update({_id:this._id},{$set:{dontShowThis:true}});
        alert("borrado");
      }
    },
    'click #editTeamMember'(){
        console.log("insert edit action here, programmer!");
    }
});


Template.mantenimiento_c.helpers({
  /*mantCollection() {
      return MantencionesDMT;
  },*/
});

Template.elementForm.helpers({
    formCollection() {
        return Repetidores;
    },
    team(){
        return Repetidores.find({});
    },
    imageFile(pictureId){
        console.log("imagefile has been called from elementform helpers");
        var fileRef = Images.collection.findOne(pictureId);
        return Images.link(fileRef, 'thumbnail','/');
    },
});

Template.appbody.onRendered(function(){

})
