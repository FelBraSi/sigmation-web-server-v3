import { MQTTcollection } from '/lib/collections/collections.js';
import { PollingDevices } from '/lib/collections/PollingDevices.js';
import { ModbusDevices } from '/lib/collections/dolf/ModbusDevices.js';
import { ModbusDeviceClass } from '/lib/collections/dolf/ModbusDeviceClass.js';
import { Config } from '/lib/config/config.js';

if (Meteor.isClient) {
  if(Config.currentsystem=="DOLF") Meteor.subscribe('modbusdeviceclass');
  if(Config.currentsystem=="DOLF") Meteor.subscribe('mbpollingdevices');
  if(Config.currentsystem=="DRS") Meteor.subscribe('pollingdevices');
}
//

var pollingDevicesHelpers = {
  formCollectionD() {
      return PollingDevices;
  },
  formCollectionM() {
      return ModbusDevices;
  },
  formCollectionMs() {
    console.log("modbusdevices");
      return ModbusDevices.find({});
  },
  formCollectionsD() {
      return PollingDevices.find({});
  },
  classOptions: function () {
                    console.log("modbusdeviceclass");
                    return ModbusDeviceClass.find().map(function (c) {
                      return {label: c.name, value: c._id};
      });

    },
  MBClass(idclass){
    var thisClass = ModbusDeviceClass.findOne({_id:idclass});
    return thisClass.name;
  }
};

Template.drspollinglistt.events({
    'click .redeployDevice'(){
      var r = confirm("Re-Deploy?");
      if (r == true) {
        PollingDevices.update({_id:this._id},{$set:{deploy:true}});
        console.log(this._id);
        alert("Deploy en curso");
      }
    },
    'click .editPollingDevice'(){
      console.log("edit");
    },
    'click .deletePollingDevice'(){
      var r = confirm("Deshabilitar?");
      if (r == true) {
        PollingDevices.update({_id:this._id},{$set:{dontShowThis:true,deploy:false}});
        console.log(this._id);
        alert("Deshabilitado");
      }
    },
    'click .enablePollingDevice'(){
      var r = confirm("Habiltar?");
      if (r == true) {
        PollingDevices.update({_id:this._id},{$set:{dontShowThis:false}});
        console.log(this._id);
        alert("Habilitado");
      }
    } ,
    'click .mqttcleanup'(){
      var r = confirm("Borrar mensajes MQTT de este Equipo? (esto borrara los mensajes solo en la interfaz, de forma permanente)");
      if (r == true) {
        //PollingDevices.update({_id:this._id},{$set:{dontShowThis:false}});
        console.log(this._id);
        Meteor.call("removeTopics", this._id);
        alert("Borrando");
      }
    }

});


Template.pollingitem.helpers(pollingDevicesHelpers);
Template.dolfpollinglistt.helpers(pollingDevicesHelpers);
Template.dolfpollinglist.helpers(pollingDevicesHelpers);
Template.drspollinglistt.helpers(pollingDevicesHelpers);
Template.drspollinglist.helpers(pollingDevicesHelpers);
Template.pollinglist.helpers(pollingDevicesHelpers);
Template.pollinglisttemplate.helpers(pollingDevicesHelpers);
