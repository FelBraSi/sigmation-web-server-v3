import { MQTTcollection } from '../lib/collections/collections.js';
import { MantencionesDMT } from '../lib/collections/mantenciones.js';
import { MantencionesSEM } from '../lib/collections/mantenciones.js';
import { MantencionesAN } from '../lib/collections/mantenciones.js';
import { Repetidores } from '../lib/collections/repetidores.js';
import { OpticPortDevices } from '../lib/collections/repetidores.js';
import { Team } from '../lib/collections/team.js';
import { Images } from '../lib/collections/images.js';
import { Supervisores } from '../lib/collections/supervisores.js';
import { PollingDevices } from '../lib/collections/PollingDevices.js';
import { ModbusDevices } from '../lib/collections/dolf/ModbusDevices.js';
import { ModbusDeviceClass } from '../lib/collections/dolf/ModbusDeviceClass.js';
import { DataGroups } from '../lib/collections/dolf/DataGroups.js';
import { ModbusVar } from '../lib/collections/dolf/ModbusVar.js';
import { RTPHistory } from '../lib/collections/drs/RTPHistory.js';
import { AlarmHistory } from '../lib/collections/alarmsCollection.js';
import { SystemVars } from '/lib/collections/systemvars.js';
import '../lib/methods.js';

if (Meteor.isServer) {

    Meteor.publish('systemvars', function() {
        return SystemVars.find({});
    });

    Meteor.publish('alarmhistory', function() {
        return AlarmHistory.find({},{sort:{"createdAt":-1}});
    });

    Meteor.publish('rtphistorybytopic', function(topicId){
        return RTPHistory.find({topicId:topicId},{limit:50000},{sort:{"createdAt":1}});
    });

    Meteor.publish('mqttcollection', function() {
        return MQTTcollection.find({});
    });

    Meteor.publish('formelement', function(id){
        return Team.find({_id:id});
    });

    Meteor.publish('myteams', function(){
        return Team.find({dontShowThis:false});
    });

    Meteor.publish('files.images.all',function() {
        return Images.collection.find({});
    });

    Meteor.publish('mantenciones_dmt',function() {
        return MantencionesDMT.find({dontShowThis:false});
    });

    Meteor.publish('mantenciones_dmt_id',function(id){
        return MantencionesDMT.find({idEquipo:id});
    });

    Meteor.publish('mantenciones_sem',function() {
        return MantencionesSEM.find();
    });

    Meteor.publish('mantenciones_sem_id',function(id){
        return MantencionesSEM.find({idEquipo:id});
    });

    Meteor.publish('mantenciones_an',function() {
        return MantencionesAN.find();
    });

    Meteor.publish('mantenciones_an_id',function(id){
        return MantencionesAN.find({idEquipo:id});
    });

    Meteor.publish('supervisores',function() {
        return Supervisores.find({dontShowThis:false});
    });

    Meteor.publish('supervisor',function(id){
        return Supervisores.find({_id:id});
    });

    Meteor.publish('repetidores',function() {
        return Repetidores.find();
    });

    Meteor.publish('repetidores_id',function(id){
        return Repetidores.find({_id:id});
    });

    Meteor.publish('opticportdevices',function() {
        return OpticPortDevices.find();
    });

    Meteor.publish('opticportdevices_id',function(id){
        return OpticPortDevices.find({_id:id});
    });

    Meteor.publish('pollingdevices',function() {
        return PollingDevices.find({});
    });

    Meteor.publish('mbpollingdevices',function() {
        return ModbusDevices.find({});
    });

    Meteor.publish('modbusdeviceclass_id',function(id) {
        return OpticPortDevices.find({_id:id});
    });

    Meteor.publish('modbusdeviceclass', function() {
      if (Roles.userIsInRole(this.userId, ["admin"],"admin-group")) {
        return ModbusDeviceClass.find();
      } else {
        return ModbusDeviceClass.find({dontShowThis:false});
      }
    });

    Meteor.publish('modbusdeviceclass_all',function() {
        return ModbusDeviceClass.find();
    });

    Meteor.publish('datagroups_id',function(id){
        return DataGroups.find({classd:id});
    });

    Meteor.publish('datagroups',function() {
        return DataGroups.find({});
    });

    Meteor.publish('modbusvar',function() {
        return ModbusVar.find({});
    });

    Meteor.publish('allusers',function() {
        return Meteor.users.find();
    });


}
